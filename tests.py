from functools import reduce
import pytest
from upload import\
    has_enough_arguments,\
    basic_validation,\
    get_credentials_flag,\
    get_credentials_path,\
    get_profile_flag,\
    get_profile,\
    get_path_to_file,\
    args_case


def setup_four_arguments():
    return ['upload', 'readme.md', '-p', 'personal', '-c', 'credentials']


def test_has_enough_arguments():
    arguments = setup_four_arguments()
    assert has_enough_arguments(arguments) is True


def test_basic_validation():
    argument = ['upload']
    with pytest.raises(Exception, message="Not enough arguments"):
        basic_validation(argument)


def test_get_credentials_flag():
    arguments = setup_four_arguments()
    assert get_credentials_flag(arguments) == '-c'


def test_get_credentials_path():
    arguments = setup_four_arguments()
    assert get_credentials_path(arguments) == 'credentials'


def test_get_profile_flag():
    arguments = setup_four_arguments()
    assert get_profile_flag(arguments) == '-p'


def test_get_profile():
    arguments = setup_four_arguments()
    assert get_profile(arguments) == 'personal'


def test_get_path_to_file():
    arguments = setup_four_arguments()
    assert get_path_to_file(arguments) == 'readme.md'


def test_args_case_full():
    args = ['upload', 'filepath', '-c', 'file', '--profile', 'personal']
    result = dict(args_case(arg, args) for arg in args if arg in ['-c', '--credentials', '-p', '--profile', 'upload'])
    assert result == {'path_to_file': 'filepath', 'credentials': 'file', 'profile': 'personal'}
