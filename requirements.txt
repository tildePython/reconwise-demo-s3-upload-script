attrs==17.3.0
boto3==1.4.7
botocore==1.7.46
click==6.7
configparser==3.5.0
docutils==0.14
jmespath==0.9.3
pex==1.2.13
pluggy==0.6.0
py==1.5.2
pytest==3.3.1
python-dateutil==2.6.1
python-dotenv==0.7.1
s3transfer==0.1.11
six==1.11.0
