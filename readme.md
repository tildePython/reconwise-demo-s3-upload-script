# About
This project aims to use upload a file to S3 using `pex` and `boto3`

`pex` is a library for generating python binaries. [Read More](https://github.com/pantsbuild/pex)

## Requirements
- Ensure you have python3 installed (preferably python3.6)

## How to use

Create an `.env` file. In here fill in the `BUCKET_NAME` and `SUBFOLDER` variables for AWS service configurations.

### Example
```
BUCKET_NAME=my-bucket
SUBFOLDER=client_subfolder
```

### Run command
Simply just run:

```
python3 python-dep.pex upload-file.py [client_name] [file_to_upload]
```

`credentials` file are formatted as per aws credentials file.

```
[default]
aws_access_key_id = my_aws_access_key_id
aws_secret_access_key = my_aws_secret_access_key
```

Change or replace them as needed.

By default, it will use a `credentials` file in the project root directory and
the `default` profile. If there's no `credentials` file. It will throw an error.

If you want to specify a particular credentials file to use with the `-c` or `--credentials` flag:

```
python3 python-dep.pex upload-file.py  [client_name] [path_to_file] -c [path_to_credentials]
```

You can specify the profile to use as well using the `-p` or `--profile` flag:

```
python3 python-dep.pex upload-file.py [path_to_credentials] [profile] [path_to_file_for_upload] -p [profile_name]
```
