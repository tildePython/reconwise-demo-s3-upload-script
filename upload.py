import boto3
import sys
import configparser

import os
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)


args = sys.argv
credentials = None
filepath = sys.argv[-1]
profile = "default"


def has_enough_arguments(arguments):
    return len(arguments) > 1


def basic_validation(arguments):
    if not has_enough_arguments(arguments):
        raise Exception('Not enough arguments')
    return list(arguments)


def put_object(params):
    s3 = boto3.resource('s3',
                        **{
                            'aws_access_key_id': params['aws_access_key_id'],
                            'aws_secret_access_key': params['aws_secret_access_key']
                        })

    data = open(params['file_path'], 'rb')
    s3.Bucket(params['bucket_name']).put_object(Key='{0}/{1}'.format(params['subfolder'], params['filename']), Body=data)


def get_credentials_flag(arguments):
    if "--credentials" in arguments:
        return "--credentials"
    elif "-c" in arguments:
        return "-c"
    return ""


def get_credentials_path(arguments):
    flag = get_credentials_flag(arguments)
    if not flag:
        return './credentials'
    index = arguments.index(flag)
    return arguments[index + 1]


def get_profile_flag(arguments):
    if "--profile" in arguments:
        return '--profile'
    elif "-p" in arguments:
        return '-p'
    return ''


def get_profile(arguments):
    flag = get_profile_flag(arguments)
    if not flag:
        return 'default'
    index = arguments.index(flag)
    return arguments[index + 1]


def get_path_to_file(arguments):
    cp = list(arguments)
    cred_flag = get_credentials_flag(cp)
    prof_flag = get_profile_flag(cp)

    if cred_flag:
        cred_flag_index = cp.index(cred_flag)
        cp.pop(cred_flag_index + 1)
        cp.pop(cred_flag_index)
    if prof_flag:
        prof_flag_index = cp.index(prof_flag)
        cp.pop(prof_flag_index + 1)
        cp.pop(prof_flag_index)
    return cp[-1]


def read_credentials_file(path, profile):
    config = configparser.ConfigParser()
    config.read(path)
    if profile not in config:
        raise Exception('No \'{0}\' profile in credentials'.format(profile))

    return {
        'aws_access_key_id': config[profile]['aws_access_key_id'],
        'aws_secret_access_key': config[profile]['aws_secret_access_key']
    }


def parse(arguments):
    credentials_path = get_credentials_path(arguments)
    profile = get_profile(arguments)
    file_path = get_path_to_file(arguments)
    filename = file_path.split('/')[-1]

    aws_keys = read_credentials_file(credentials_path, profile)

    params = {
        **aws_keys,
        'file_path': file_path,
        'bucket_name': os.environ.get('BUCKET_NAME'),
        'subfolder': os.environ.get('SUBFOLDER'),
        'filename': filename
    }

    return params


def args_case(arg, args):
    """
    Work in progress, refactoring to follow functional programming principals
    """
    if arg in ['-p', '--profile']:
        return ('profile', args[args.index(arg) + 1])
    if arg in ['-c', '--credentials']:
        return ('credentials', args[args.index(arg) + 1])
    if arg in ['upload']:
        return ('path_to_file', args[args.index(arg) + 1])
    return ''


def get_profile_or_credential(arg):
    """
    Work in progress, refactoring to follow functional programming principals
    """
    return arg if arg in ["-p", "--profile"] else None


def upload(arguments):
    try:
        if not basic_validation(arguments):
            raise Exception('Not enough arguments')
        params = parse(arguments)
        put_object(params)

        return {**params}
    except Exception as e:
        print(str(e))
